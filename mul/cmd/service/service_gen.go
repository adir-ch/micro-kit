// THIS FILE IS AUTO GENERATED BY GK-CLI DO NOT EDIT!!
package service

import (
	endpoint1 "github.com/go-kit/kit/endpoint"
	log "github.com/go-kit/kit/log"
	prometheus "github.com/go-kit/kit/metrics/prometheus"
	opentracing "github.com/go-kit/kit/tracing/opentracing"
	http "github.com/go-kit/kit/transport/http"
	nats "github.com/go-kit/kit/transport/nats"
	group "github.com/oklog/oklog/pkg/group"
	opentracinggo "github.com/opentracing/opentracing-go"
	endpoint "gitlab.com/adir-ch/dcalc/mul/pkg/endpoint"
	http1 "gitlab.com/adir-ch/dcalc/mul/pkg/http"
	service "gitlab.com/adir-ch/dcalc/mul/pkg/service"
)

func createService(endpoints endpoint.Endpoints) (g *group.Group) {
	g = &group.Group{}
	//initNatsHandler(endpoints, g)
	initHttpHandler(endpoints, g)
	return g
}
func defaultHttpOptions(logger log.Logger, tracer opentracinggo.Tracer) map[string][]http.ServerOption {
	options := map[string][]http.ServerOption{"Mul": {http.ServerErrorEncoder(http1.ErrorEncoder), http.ServerErrorLogger(logger), http.ServerBefore(opentracing.HTTPToContext(tracer, "Mul", logger))}}
	return options
}

func defaultNatsOptions(logger log.Logger /*, tracer opentracinggo.Tracer*/) []nats.SubscriberOption {
	options := []nats.SubscriberOption{
		nats.SubscriberErrorEncoder(nats.DefaultErrorEncoder),
		nats.SubscriberErrorLogger(logger),
	}
	return options
}

func addDefaultEndpointMiddleware(logger log.Logger, duration *prometheus.Summary, mw map[string][]endpoint1.Middleware) {
	mw["Mul"] = []endpoint1.Middleware{endpoint.LoggingMiddleware(log.With(logger, "method", "Mul")), endpoint.InstrumentingMiddleware(duration.With("method", "Mul"))}
}
func addDefaultServiceMiddleware(logger log.Logger, mw []service.Middleware) []service.Middleware {
	return append(mw, service.LoggingMiddleware(logger))
}
func addEndpointMiddlewareToAllMethods(mw map[string][]endpoint1.Middleware, m endpoint1.Middleware) {
	methods := []string{"Mul"}
	for _, v := range methods {
		mw[v] = append(mw[v], m)
	}
}
