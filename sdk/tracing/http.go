package tracing

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"strconv"

	kitlog "github.com/go-kit/kit/log"
	log "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
	opentracing "github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
)

func SpanFromReq(tracer opentracing.Tracer, operationName string, logger kitlog.Logger) kithttp.RequestFunc {
	return func(ctx context.Context, req *http.Request) context.Context {
		// Try to join to a trace propagated in `req`.
		var span opentracing.Span
		wireContext, err := tracer.Extract(
			opentracing.TextMap,
			opentracing.HTTPHeadersCarrier(req.Header),
		)
		if err != nil && err != opentracing.ErrSpanContextNotFound {
			logger.Log("err", err)
		}

		span = tracer.StartSpan(operationName, ext.RPCServerOption(wireContext))
		ext.HTTPMethod.Set(span, req.Method)
		ext.HTTPUrl.Set(span, req.URL.String())
		logger.Log(operationName, "span started", "data", fmt.Sprintf("span: %v", span))
		return opentracing.ContextWithSpan(ctx, span)
	}
}

func SpanFinishRes(name string, tracer opentracing.Tracer, logger kitlog.Logger) kithttp.ServerResponseFunc {
	return func(ctx context.Context, w http.ResponseWriter) context.Context {
		span := opentracing.SpanFromContext(ctx)
		logger.Log(name, "span finished", "data", fmt.Sprintf("span: %v", span))
		span.Finish()
		return ctx
	}
}

func SpanFinish(name string, tracer opentracing.Tracer, logger kitlog.Logger) kithttp.ServerFinalizerFunc {
	return func(ctx context.Context, code int, r *http.Request) {
		span := opentracing.SpanFromContext(ctx)
		logger.Log(name, "span finalized")
		span.Finish()
	}
}

func InjectContextToHTTP(ctx context.Context, tracer opentracing.Tracer, logger log.Logger, req *http.Request) error {
	// Try to find a Span in the Context.
	if span := opentracing.SpanFromContext(ctx); span != nil {
		// Add standard OpenTracing tags.
		//ext.HTTPMethod.Set(span, req.Method)
		ext.HTTPUrl.Set(span, req.URL.String())
		host, portString, err := net.SplitHostPort(req.URL.Host)
		if err == nil {
			ext.PeerHostname.Set(span, host)
			if port, err := strconv.Atoi(portString); err != nil {
				ext.PeerPort.Set(span, uint16(port))
			}
		} else {
			ext.PeerHostname.Set(span, req.URL.Host)
		}

		// There's nothing we can do with any errors here.
		if err = tracer.Inject(
			span.Context(),
			opentracing.TextMap,
			opentracing.HTTPHeadersCarrier(req.Header),
		); err != nil {
			logger.Log("calc", "span inject", "err", err)
		}
	}
	return nil
}
