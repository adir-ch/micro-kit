package service

import (
	"context"
	"fmt"

	log "github.com/go-kit/kit/log"
	"github.com/google/uuid"
	"github.com/opentracing/opentracing-go"
)

// CalcService describes the service.
type CalcService interface {
	// Calculate - evaluate a math expression and return the result
	Calculate(ctx context.Context, expr string) (rs float64, err error)
}

type basicCalcService struct {
	tracer opentracing.Tracer
	logger log.Logger
}

func (b *basicCalcService) Calculate(ctx context.Context, expr string) (rs float64, err error) {
	u := uuid.New()
	l := log.With(b.logger, "req-uuid", u.String())
	if span := opentracing.SpanFromContext(ctx); span != nil {
		l.Log("root-span", fmt.Sprintf("%v", span), "UUID", u)
		span.LogKV("calc request UUID", u)
	}
	rs, err = eval(ctx, b.tracer, l, expr)
	return rs, err
}

// NewBasicCalcService returns a naive, stateless implementation of CalcService.
func NewBasicCalcService(tracer opentracing.Tracer, logger log.Logger) CalcService {
	return &basicCalcService{
		tracer: tracer,
		logger: logger,
	}
}

// New returns a CalcService with all of the expected middleware wired in.
func New(tracer opentracing.Tracer, logger log.Logger, middleware []Middleware) CalcService {
	var svc = NewBasicCalcService(tracer, logger)
	for _, m := range middleware {
		svc = m(svc)
	}
	return svc
}
