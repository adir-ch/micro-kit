package service

import (
	"context"
	"fmt"

	log "github.com/go-kit/kit/log"
	"github.com/opentracing/opentracing-go"
	discovery "gitlab.com/adir-ch/dcalc/sdk/discovery"
)

type OpService struct {
	Name   string
	Client ClientFunc
}

var ops = map[string]OpService{ // TODO: take from config
	"+": OpService{"services/add/", httpSend},
	"-": OpService{"services/sub/", grpcSendSub},
	"*": OpService{"", reqRepSendMul},
	"/": OpService{"services/div/", httpSend},
}

type svcreq struct {
	Numbers []float64 `json:"numbers,omitempty"`
}

type svcres struct {
	Rs  float64 `json:"rs"`
	Err error   `json:"err"`
}

func callOp(ctx context.Context, tracer opentracing.Tracer, logger log.Logger, op string, numbers []float64) (float64, error) {
	svc, found := ops[op]
	if found == false {
		return 0, fmt.Errorf("unable to find service for op: %s", op)
	}

	srvURI := ""
	var err error
	if svc.Name != "" {
		srvURI, err = discovery.EtcdDiscover(svc.Name)
		if err != nil {
			return 0, err
		}
	}

	return svc.Client(ctx, tracer, logger, svcreq{numbers}, srvURI)
}
