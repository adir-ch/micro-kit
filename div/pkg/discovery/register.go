package discovery

import (
	"context"
	"fmt"
	"log"

	"go.etcd.io/etcd/client"
)

var (
	etcdServer = "http://etcd:2379"
	prefix     = "/services/div/"
	instance   = "div:8081"
	key        = prefix + instance
)

func EtcdRegister() (client.KeysAPI, error) {
	cfg := client.Config{Endpoints: []string{etcdServer}}
	c, err := client.New(cfg)
	if err != nil {
		log.Fatal(err)
	}

	kapi := client.NewKeysAPI(c)
	resp, err := kapi.Set(context.Background(), key, instance, nil)
	if err != nil {
		return nil, fmt.Errorf("etcd register error: %s", err)
		// if err == context.Canceled {
		// 	// ctx is canceled by another routine
		// } else if err == context.DeadlineExceeded {
		// 	// ctx is attached with a deadline and it exceeded
		// } else if cerr, ok := err.(*client.ClusterError); ok {
		// 	// process (cerr.Errors)
		// } else {
		// 	// bad cluster endpoints, which are not etcd servers
		// }
	}

	// print common key info
	log.Printf("etcd set is done. Metadata is %q\n", resp)
	return kapi, nil
}

func EtcdDeregister(kapi client.KeysAPI) {
	resp, err := kapi.Delete(context.Background(), key, nil)
	if err != nil {
		fmt.Printf("error while de-registering div: %s", err)
		// if err == context.Canceled {
		// 	// ctx is canceled by another routine
		// } else if err == context.DeadlineExceeded {
		// 	// ctx is attached with a deadline and it exceeded
		// } else if cerr, ok := err.(*client.ClusterError); ok {
		// 	// process (cerr.Errors)
		// } else {
		// 	// bad cluster endpoints, which are not etcd servers
		// }
	}
	// print common key info
	log.Printf("service delete is done. Metadata is %q\n", resp)
}
