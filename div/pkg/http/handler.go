package http

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	opentracing "github.com/opentracing/opentracing-go"
	opentracinglog "github.com/opentracing/opentracing-go/log"
	eps "gitlab.com/adir-ch/dcalc/div/pkg/endpoint"
)

// DivHandler - main Div handler
type DivHandler struct {
	Endpoint eps.Endpoint
	tracer   opentracing.Tracer
}

// NewDivHandler - creates a new Div HTTP handler
func NewDivHandler(ep eps.Endpoint, tracer opentracing.Tracer) *DivHandler {
	return &DivHandler{
		Endpoint: ep,
		tracer:   tracer,
	}
}

func (handler DivHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method == "OPTIONS" { //handle CORS option req
		HandlePreFlightMessage(w, r)
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		handleError(http.StatusBadRequest, err, w)
		return
	}

	requestData := eps.DivRequest{}
	if len(body) > 0 {
		err = json.Unmarshal(body, &requestData)
		if err != nil {
			handleError(http.StatusBadRequest, err, w)
			return
		}
	}

	msg := fmt.Sprintf("div request: %+v", requestData)
	fmt.Println(msg)
	span := handler.tracer.StartSpan("Div")
	defer span.Finish()
	span.SetTag("numbers", requestData.Numbers)
	span.Log(opentracing.LogData{Timestamp: time.Now(), Event: "div request", Payload: msg})
	res := handler.Endpoint.Div(&requestData)
	encoder := json.NewEncoder(w)
	if err != nil {
		handleError(http.StatusBadRequest, err, w)
		span.LogFields(opentracinglog.String("event", "Div"), opentracinglog.String("value", err.Error()))
		span.Finish()
		return
	}

	span.LogFields(opentracinglog.String("event", "Div"), opentracinglog.String("value", fmt.Sprintf("%.2f", res.Rs)))
	span.LogKV("event", "Div")
	encoder.Encode(res)
}

// HandlePreFlightMessage - CORS handler
func HandlePreFlightMessage(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("Got options request from client\n")
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
}

func handleError(code int, err error, w http.ResponseWriter) {
	fmt.Printf("Error on Div request: %s\n", err.Error())
	//http.Error(w, err.Error(), code)
	w.WriteHeader(code)
	w.Write([]byte(err.Error()))
}
