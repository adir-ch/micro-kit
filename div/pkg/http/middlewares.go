package http

import (
	"fmt"
	"net/http"
	"time"

	opentracing "github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
)

// RequestPacer - log each incoming request
func RequestPacer(h http.Handler) http.HandlerFunc {
	f := func(w http.ResponseWriter, r *http.Request) {
		fmt.Printf("<----- REQUEST: Div req received from: %s\n", r.RemoteAddr)
		start := time.Now()
		h.ServeHTTP(w, r)
		fmt.Printf("-----> RESPONSE: Div res finished (timed %s)\n",
			(fmt.Sprint(time.Since(start).Nanoseconds()/1000000) + "ms."))
	}

	return http.HandlerFunc(f)
}

// RequestTracer - handle request tracing
func RequestTracer(h http.Handler, tracer opentracing.Tracer) http.HandlerFunc {
	f := func(w http.ResponseWriter, r *http.Request) {
		// Try to join to a trace propagated in `req`.
		var span opentracing.Span
		wireContext, err := tracer.Extract(
			opentracing.TextMap,
			opentracing.HTTPHeadersCarrier(r.Header),
		)
		if err != nil && err != opentracing.ErrSpanContextNotFound {
			fmt.Printf("span not found, err: %s\n", err)
		} else {
			span = tracer.StartSpan("div", ext.RPCServerOption(wireContext))
			defer span.Finish()
			ext.HTTPMethod.Set(span, r.Method)
			ext.HTTPUrl.Set(span, r.URL.String())
			fmt.Printf("div span started: %v\n", span)
		}

		h.ServeHTTP(w, r)
	}

	return http.HandlerFunc(f)
}
