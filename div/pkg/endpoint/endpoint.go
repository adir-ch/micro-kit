package endpoint

import (
	"fmt"

	"gitlab.com/adir-ch/dcalc/div/pkg/service"
)

// Endpoint - basic div endpoint
type Endpoint interface {
	Div(request *DivRequest) *DivResponse
}

// DivRequest - div request
type DivRequest struct {
	Numbers []float64 `json:"numbers"`
}

// DivResponse - div response
type DivResponse struct {
	Rs  float64 `json:"rs,omitempty"`
	Err error   `json:"err,omitempty"`
}

// DivEndpoint - basic endpoint
type DivEndpoint struct {
	service service.Service
}

// NewDivEndpoint - Creates a new endpoint
func NewDivEndpoint(s service.Service) Endpoint {
	return DivEndpoint{service: s}
}

// Div - Div request handle
func (e DivEndpoint) Div(request *DivRequest) *DivResponse {
	if len(request.Numbers) == 0 {
		fmt.Printf("no items found on request")
		return &DivResponse{Rs: 0, Err: fmt.Errorf("no items in request")}
	}

	total, err := e.service.Div(request.Numbers)
	if err != nil {
		return &DivResponse{Rs: 0, Err: err}
	}

	return &DivResponse{Rs: total}
}
