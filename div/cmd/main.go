package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	kitlog "github.com/go-kit/kit/log"
	opentracing "github.com/opentracing/opentracing-go"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/adir-ch/dcalc/div/pkg/discovery"
	"gitlab.com/adir-ch/dcalc/div/pkg/endpoint"
	transport "gitlab.com/adir-ch/dcalc/div/pkg/http"
	"gitlab.com/adir-ch/dcalc/div/pkg/service"
	tracing "gitlab.com/adir-ch/dcalc/sdk/tracing"
	etcd "go.etcd.io/etcd/client"
)

var (
	Version string
	Build   string
	logger  *log.Logger
)

func startHTTPServer(port int, mux *http.ServeMux) error {
	fmt.Println("Starting Backend service listener")

	fmt.Printf("Starting Server on port %v\n", port)
	address := fmt.Sprintf(":%d", port)
	err := http.ListenAndServe(address, mux)
	if err != nil {
		fmt.Printf("Cannot start Server on port %v - %s\n", port, err)
	}

	return err
}

func register(h http.Handler) *http.ServeMux {
	mux := http.NewServeMux()
	mux.Handle("/calc", h)
	mux.Handle("/metrics", promhttp.Handler())
	return mux
}

func initDivService(t opentracing.Tracer) http.Handler {
	// div service middlewares
	middlerwares := []service.ServiceMiddleware{
		//co.GetDomainEventMw(log.New(os.Stdout, "dcalc: ", log.LstdFlags)),
		service.GetDomainEventMwFunc(log.New(os.Stdout, "Domain Event: ", log.LstdFlags)),
		service.GetProcessTimingMw(),
		service.GetReqMetricsMw(promauto.NewCounter(prometheus.CounterOpts{
			Namespace: "dcalc",
			Name:      "div_processed_ops_total",
			Help:      "The total number of processed requests"},
		)),
	}

	// create a new div service
	svc := service.New(middlerwares)

	durations := promauto.NewSummaryVec(prometheus.SummaryOpts{
		Namespace:  "dcalc",
		Name:       "div_op_req_duration_sec",
		Help:       "How much time in seconds a div op req took",
		Objectives: map[float64]float64{0.5: 0.05, 0.9: 0.01, 0.99: 0.001},
	},
		[]string{"method", "success"},
	)

	// plug the service to the instrumented endpoint
	// ep := endpoint.GetEndpointDurationMw(durations)(endpoint.NewDivEndpoint(svc))
	divep := endpoint.NewDivEndpoint(svc)
	epmw := endpoint.GetEndpointDurationMw(durations)
	ep := epmw(divep)

	// create a new handler & plug the ep to the handler
	handler := transport.NewDivHandler(ep, t)
	return transport.RequestTracer(transport.RequestPacer(handler), t)
}

func discoveryRegister() (etcd.KeysAPI, error) {
	client, err := discovery.EtcdRegister()
	if err != nil {
		fmt.Printf("unable to register service: %s", err)
		return nil, err
	}

	return client, nil
}

func main() {
	fmt.Printf("starting dcalc (version: %s, build: %s)\n", Version, Build)

	logger := kitlog.NewLogfmtLogger(os.Stdout)

	tracer, closer := tracing.InitJaegerCollector(os.Getenv("JAEGER_SERVICE_NAME"), logger)
	opentracing.SetGlobalTracer(tracer)
	defer closer.Close()

	registrar, err := discoveryRegister()
	if err != nil {
		fmt.Printf("unable to start div service: %s", err)
	}

	defer discovery.EtcdDeregister(registrar)

	co := initDivService(tracer)
	mux := register(co)
	if err := startHTTPServer(8081, mux); err != nil {
		log.Panic(err)
	}
}
