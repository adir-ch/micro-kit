package main

import service "gitlab.com/adir-ch/dcalc/add/cmd/service"

var Version string
var Build string

func main() {
	service.Run(Version, Build)
}
