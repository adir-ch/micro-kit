package service

import (
	"context"
	"math/rand"
	"os"
	"reflect"
	"testing"
	"testing/quick"
	"time"
)

var svc *basicAddService
var ctx context.Context

func TestMain(m *testing.M) { // use if needs to init something
	svc = new(basicAddService)
	ctx = context.Background()
	m.Run()
	os.Exit(0)
}

func Test_basicAddService_Add(t *testing.T) {
	type args struct {
		ctx     context.Context
		numbers []float64
	}
	tests := []struct {
		name    string
		b       *basicAddService
		args    args
		wantRs  float64
		wantErr bool
	}{
		{"no numbers", svc, args{ctx, []float64{}}, 0, false},
		{"some int numbers", svc, args{ctx, []float64{1, 1}}, 2, false},
		{"some float numbers", svc, args{ctx, []float64{1.1, 1.1}}, 2.2, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotRs, err := tt.b.Add(tt.args.ctx, tt.args.numbers)
			if (err != nil) != tt.wantErr {
				t.Errorf("basicAddService.Add() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotRs != tt.wantRs {
				t.Errorf("basicAddService.Add() = %v, want %v", gotRs, tt.wantRs)
			}
		})
	}
}

func Test_Blackbox_basicAddService_Add(t *testing.T) {
	var err error
	f := func(numbers []float64) bool {
		var r float64
		r, err = svc.Add(context.Background(), numbers)
		if err != nil {
			return false
		}

		var sum float64
		for _, n := range numbers {
			sum += n
		}
		if r != sum {
			return false
		}

		return true
	}

	if e := quick.Check(f, nil); e != nil {
		t.Error(err)
		t.Error(e)
	}
}

/////// Configure own generator to generate numbers
type InputNum float64

func (f InputNum) Generate(rand *rand.Rand, size int) reflect.Value {
	result := rand.Float64()*20. - 10.
	return reflect.ValueOf(result)
}

func inputValues(values []reflect.Value, randomizer *rand.Rand) {
	for i := 0; i < len(values); i++ {
		val, ok := quick.Value(reflect.TypeOf(InputNum(1.)), randomizer)
		if ok {
			values[i] = val
		}
	}
}

func Test_BlackboxGenerator_basicAddService_Add(t *testing.T) {
	var err error

	// Limitation - cannot use array here, since testing quick package will
	// try and find the 'f' number of input args, and []numbers is just one arg
	f := func(n1, n2, n3 float64) bool {
		var r float64
		r, err = svc.Add(context.Background(), []float64{n1, n2, n3})
		if err != nil {
			return false
		}

		if r != (n1 + n2 + n3) {
			return false
		}
		return true
	}

	config := quick.Config{
		MaxCount:      0,
		MaxCountScale: 0,
		Rand:          rand.New(rand.NewSource(time.Now().Unix())),
		Values:        inputValues,
	}

	if e := quick.Check(f, &config); e != nil {
		t.Error(err)
		t.Error(e)
	}
}

func Benchmark_basicAddService_Add(b *testing.B) {
	svc := basicAddService{}
	//b.ReportAllocs() -- can be called with flag to bench test
	b.ResetTimer() // the creation of the service is not part of the bench
	for i := 0; i < b.N; i++ {
		svc.Add(context.Background(), []float64{1., 1.})
	}
}

func Benchmark_basicAddService_Add_parallel(b *testing.B) {
	b.SetParallelism(32) // number of go routines
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			svc.Add(context.Background(), []float64{1., 1.})
		}
	})
}
