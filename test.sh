#! /bin/sh

echo "1 + 1 = $(curl -s -d '{"expr": "1 + 1"}' -X POST localhost:8801/calculate)"
echo "1 - 1 = $(curl -s -d '{"expr": "1 - 1"}' -X POST localhost:8801/calculate)"
echo "1 * 1 = $(curl -s -d '{"expr": "1 * 1"}' -X POST localhost:8801/calculate)"
echo "1 / 1 = $(curl -s -d '{"expr": "1 / 1"}' -X POST localhost:8801/calculate)"
echo "1 + 1 - 5 = $(curl -s -d '{"expr": "1 + 1 - 5"}' -X POST localhost:8801/calculate)"
echo "1 + 1 * 2 - 3 = $(curl -s -d '{"expr": "1 + 1 * 2 - 3"}' -X POST localhost:8801/calculate)"
echo "1 + 1 * 2 - 3 / 5 = $(curl -s -d '{"expr": "1 + 1 * 2 - 3 / 5"}' -X POST localhost:8801/calculate)"
echo "((1 + 1 * 2 - (3 / 5) * 2 + 4) / 3) = $(curl -s -d '{"expr": "((1 + 1 * 2 - (3 / 5) * 2 + 4) / 3)"}' -X POST localhost:8801/calculate)"
echo "((1 + 1 * 2 - (3 / 5) * 2 + 4) / 3) + ((1 + 1 * 2 - (3 / 5) * 2 + 4) / 3) + ((1 + 1 * 2 - (3 / 5) * 2 + 4) / 3) + ((1 + 1 * 2 - (3 / 5) * 2 + 4) / 3) + ((1 + 1 * 2 - (3 / 5) * 2 + 4) / 3) + ((1 + 1 * 2 - (3 / 5) * 2 + 4) / 3)= $(curl -s -d '{"expr": "((1 + 1 * 2 - (3 / 5) * 2 + 4) / 3) + ((1 + 1 * 2 - (3 / 5) * 2 + 4) / 3) + ((1 + 1 * 2 - (3 / 5) * 2 + 4) / 3) + ((1 + 1 * 2 - (3 / 5) * 2 + 4) / 3) + ((1 + 1 * 2 - (3 / 5) * 2 + 4) / 3) + ((1 + 1 * 2 - (3 / 5) * 2 + 4) / 3)"}' -X POST localhost:8801/calculate)"
echo "777 + 2 - 3 * 2 / 2" = "$(curl -s -d '{"expr": "10 + 2 - 3 * 2 / 2"}' -X POST localhost:8801/calculate)"